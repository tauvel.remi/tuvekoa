from django.db import models

""" User model and model manager """
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class UserManager(BaseUserManager):
    """ User Model Manager """
    def create_user(self, password=None, is_active=True, is_staff=False, is_admin=False):
        """ create user """
        # authentification keys verification
        if not password:
            raise ValueError('User must have Password')

        user_obj = self.model(
            name=name
        )

        user_obj.set_password(password)

        user_obj.is_active = is_active
        user_obj.is_staff = is_staff
        user_obj.is_admin = is_admin
        user_obj.save()
        return user_obj


    def create_superuser(self, email, password=None):
        """ create super user """
        user = self.create_user(
            password=password,
            is_staff=True,
            is_admin=True
        )
        return user

class User(AbstractBaseUser):
    ''' user model '''
    name = models.CharField(max_length=32, blank=True, null=True)
    # TODO AVATAR ?

    # registration create and update fields
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # flag to delete user without deleting it form database
    is_active = models.BooleanField(default=True)

    # permissions
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    #use name for authentification
    USERNAME_FIELD = 'name'

    def __str__(self):
        """ string representing the user """
        return self.name

    def has_perm(self, perm, obj=None):
        """ returns whether a user has permissions """
        return True

    def has_module_perms(self, app_label):
        """ returns whether a user has module perms """
        return True