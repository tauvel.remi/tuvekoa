from .base import *

# stuff that has absolutely to be set differently in prod
SECRET_KEY = "django-insecure-4w&eb9%9ab7yy)jd#q(_%^cpxdfe3nsvg%6mj4xdfvqoh-epy_"
DEBUG = True
ALLOWED_HOSTS = []


INSTALLED_APPS += ["debug_toolbar", "django_browser_reload"]

MIDDLEWARE += [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django_browser_reload.middleware.BrowserReloadMiddleware",
]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}