import os
from .base import *

DEBUG = False

ALLOWED_HOSTS = ["127.0.0.1", "localhost"]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("DB_NAME"),
        "USER":os.environ.get("DB_USER"),
        "HOST":os.environ.get("DB_HOST"),
        "PASSWORD":os.environ.get("DB_PWD"),
        "PORT":os.environ.get("DB_PORT"),
    }
}